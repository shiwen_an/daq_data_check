# Author: Shiwen An 
# Date: 2021.11.25
# Purpose: Some Function help with read the xray part of the 
#          Code and find the correlation of Xray scan with the TDAC value

import json
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import deque

# The point got zero hit or no value

# font = {'family' : 'monospace',
#         'weight' : 'bold',
#         'size' : 25
#         }

# matplotlib.rc('font', **font)
# plt.style.use('slides')
# plt.style.use('print')


def xray_occ(chips, limit):
    # Find out the problem on Occupancy map
    b2 = []
    all_chips = deque()
    d= []
    # with open(file_s[2]+chip_config[1]) as json_file:
    try:
        for x in chips:

            with open(x) as json_file:
                occ = json.load(json_file)
            occ_map = occ.get('Data')
            for i, v in enumerate(occ_map): 
                for j, k in enumerate(v):
                    if k <limit : 
                        d.append(k)
                    else:
                        d.append(limit)
                b2.append(d)
                d = []
            b2 = np.transpose(b2)
            b2 = np.flip(b2, 0)
            all_chips.append(b2)
            b2 = []
    except FileNotFoundError:
        print("[Xray ] Catch FileNotFoundError")

    return all_chips
 

def plot_xray_occupancy(chips):
    # Find out the problem on Occupancy map
    b2 = []
    d = []
    all_chips = deque()
    cnt_ = 0
    # with open(file_s[2]+chip_config[1]) as json_file:
    for x in chips:
        with open(x) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')
        for i, v in enumerate(occ_map): 
            # if i >=264 and i<399: 
            if i >=264: 
                for i, v in enumerate(v):
                    if v == 0: 
                        d.append(1)
                        cnt_ = cnt_ + 1
                    else:
                        d.append(0)
                b2.append(d)
                d = []
        
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)
        b2 = []
        print('Xray Zero: ', cnt_)
        print('Xray Zero Percentage: ', cnt_/26112) # 192*136
        cnt_ = 0

    ss = chips[0]
    plt.suptitle(ss[0:6]+' Xray Occupancy Map \n')
    plt.subplot(221)
    plt.imshow(all_chips[0], interpolation='none')
    plt.title('Chip1 Diff')
    plt.subplot(223)
    plt.imshow(all_chips[1], interpolation='none')
    plt.title('Chip2 Diff')
    plt.subplot(224)
    plt.imshow(all_chips[2], interpolation='none')
    plt.title('Chip3 Diff')
    plt.subplot(222)
    plt.imshow(all_chips[3], interpolation='none')  
    plt.title('Chip4 Diff')  
    plt.show()
    return all_chips

def correlations(chips,chip_configs):
    cnt_xray = 0
    cnt_xray_tdac = 0
    cnt_percent = []
    b = []
    b1 = []
    c = []
    d = []
    b2 = []
    d = []
    all_chips = deque()

    for x1, x2 in zip(chips,chip_configs):
        with open(x1) as json_file:
            occ = json.load(json_file)
        occ_map = occ.get('Data')

        with open(x2) as json_file:
            data_before = json.load(json_file)
        tdac_map = data_before.get('RD53A')
        tdac_map = tdac_map.get('PixelConfig')

        for i, v in enumerate(occ_map): 
            if i >=128 : 
                tdac_col = tdac_map[i]
                tdac_col = tdac_col['TDAC']
                for i, v in enumerate(v):
                    # if v == 0:
                    if v < 1: 
                        cnt_xray = cnt_xray + 1
                        if tdac_col[i] == -15 or tdac_col[i] == -15:
                            cnt_xray_tdac = cnt_xray_tdac+1
                            d.append(1)
                        else:
                            d.append(0)
                    else:
                        d.append(0)
                b2.append(d)
                d = []
        
        b2 = np.transpose(b2)
        b2 = np.flip(b2, 0)
        all_chips.append(b2)        
        b2 = []
        print("No data pixels: ")
        print(cnt_xray_tdac)
        print(cnt_xray)
        print(cnt_xray_tdac/cnt_xray)
        cnt_percent.append(cnt_xray_tdac/cnt_xray)
        cnt_xray = 0
        cnt_xray_tdac = 0

    plt.suptitle('Xray Occupancy && Chip TDAC Config[+/-15] Map')
    plt.subplot(221)
    plt.imshow(all_chips[0], interpolation='none')
    plt.title('Chip1 Lin Diff')
    
    plt.subplot(223)
    plt.imshow(all_chips[1], interpolation='none')
    plt.title('Chip2 Lin Diff')
    plt.subplot(224)
    plt.imshow(all_chips[2], interpolation='none')
    plt.title('Chip3 Lin Diff')
    plt.subplot(222)
    plt.imshow(all_chips[3], interpolation='none')  
    plt.title('Chip4 Lin Diff')  
    plt.show()

# A Slider window to increase the view of Xray Occupancy