#!/bin/sh
# Comment
# Author: Shiwen An
# Data: 2021.10.29

helpFunction()
{
   echo "Usage: ./datainspection.sh -m <ModuleType> -f <FrontEndType> "
   echo "Module type: rd53a rd53b"
   echo "Scan type: digital analog threshold crosstalk discbump"
   echo "Can be used individually or in more general case"
   echo "For overall image please check more on the "
   exit 1
}

echo "=============================================="
echo "====Data Inspection for all types of scans===="
echo "=============================================="

# python3 endf_read.py
# root test_Al.cxx
# modify the test based on type of material 
# python3 sequence.py -i xcom_Al.txt -o ../run2_E.mac

while getopts "m:f:" arg;
do
   case "${arg}" in
      m ) ModuleType=${OPTARG} 
	  echo $ModuleType
	  ;;
      f ) FrontEnd=${OPTARG} 
     echo ""$FrontEnd" Front End" 
     ;;
      ? ) helpFunction;;
   esac
done  

# Useful oneline command for checking the source directory
# I do not know why it works but stackoverflow is amazing
# Credit to this page:
# https://stackoverflow.com/questions/59895/how-can-i-get-the-source-directory-of-a-bash-script-from-within-the-script-itsel
#echo "The script you are running has basename `basename "$0"`, dirname `dirname "$0"`"
#echo "The present working directory is `pwd`"
#SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

BASENAME="`basename "$0"`"
SCRIPT_DIR="`dirname "$0"`"
ScanLog="`pwd`/scanLog.json"
# echo $BASENAME
# echo $SCRIPT_DIR
if [ ! -e $ScanLog ]; then
   echo "[ScanLog File] NOT exist"
   exit
fi
echo "[ScanLog File] "$ScanLog""
echo "[ScanLog File] Loading"
FUNCTIONNAME=""$SCRIPT_DIR"/loadScan.py"
python3 $FUNCTIONNAME -i $ScanLog 
# python3 $FUNCTIONNAME -i $ScanLog -f $FrontEnd



if [ "$material" = "Delrin" ]
then
python3 sequence.py -i xcom_CH2O.txt -o ../run2_E.mac -m Delrin
cd ../src
rm HNDetectorConstruction.cc
cat Delrin.cc >> HNDetectorConstruction.cc
cd ..
make -j3
./xray run2.mac
cd ana
cp ../hit_result.txt .
mv hit_result.txt hit_result_Delrin.txt
root test_Delrin.cxx
fi