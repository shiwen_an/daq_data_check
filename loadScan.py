# Author: Shiwen An 
# Date: 2021/12/3
# Purpose: Quickly Load the data from 
#           The input json file

import sys, getopt
import pandas as pd
from pandas.core import indexing
import json

from Xray import xray_occ
from xray_viewer import xray_viewer_run
from os.path import exists

def main(argv):
    inputfile = ''
    frontEndPart = ''
    try:
        opts, args = getopt.getopt(argv,"hi:f:",["ifile=","frontEnd="])
    except getopt.GetoptError:
        print('loadScan.py -i <inputfile> -f <frontend>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('loadScan.py -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg   
        elif opt in ("-f", "--frontEnd"):
            frontEndPart = arg
    
    print('[ScanLog File] Input file:', inputfile)

    # should be only DIFF/LIN/SYN/ALL
    print('[ScanLog File] Front End:', frontEndPart)

    with open(inputfile) as json_file:
        scanLog = json.load(json_file)
    
    scanType = scanLog.get('testType')
    inputfolder = inputfile[:len(inputfile)-len('scanLog.json')]
    
    print("[ScanLog File] Input Folder: ", inputfolder)
    print("[ScanLog File] Input Scan Type: ", scanType)

    Occupancy = ['chip1_Occupancy.json','chip2_Occupancy.json','chip3_Occupancy.json','chip4_Occupancy.json']

    if scanType == 'std_xrayscan' :
        print("[ScanLog File] Check Occupancy Map") # 
        occ = []
        for x in Occupancy:
            a = inputfolder+x
            if exists(a):
                print("Great!")
                print("[ScanLog File] Exist:", a)
                occ.append(a)
        maps = xray_occ(occ, 500)
        for x in maps:
            xray_viewer_run(x)






#    fo = open(outputfile,"w")
#    fi = open(inputfile)
#    print(fi)
    
     
if __name__ == "__main__":
   main(sys.argv[1:])
