import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib.widgets import RangeSlider
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Xray import xray_occ

def combine_string(s, b):
    c = []
    for x in b:
        c.append(s+x)
    return c

def xray_viewer_run(map):
    img = map
    fig, axs = plt.subplots(1, 2, figsize=(15, 5))
    plt.subplots_adjust(bottom=0.25)

    im = axs[0].imshow(img)

    axs[0].set_title("Hit Map Amptek Mini Xray")
    axs[1].hist(img.flatten(), bins='auto')
    axs[1].set_title('Histogram # of Hits')

    # Create the RangeSlider
    slider_ax = plt.axes([0.20, 0.15, 0.60, 0.03])
    slider = RangeSlider(slider_ax, "Number of Hits", img.min(), img.max())

    # Create two other RangeSliders for specific area and its plot
    slider_x_ax = plt.axes([0.20, 0.10, 0.60, 0.03])
    slider_y_ax = plt.axes([0.20, 0.05, 0.60, 0.03])
    slider_x = RangeSlider(slider_x_ax, "X-axis", 0.0, float(len(map[0])) )
    slider_y = RangeSlider(slider_y_ax, "Y-axis", 0.0, float(len(map)) )

    # Create the Vertical lines on the histogram
    lower_limit_line = axs[1].axvline(slider.val[0], color='k')
    upper_limit_line = axs[1].axvline(slider.val[1], color='k')

    def update(val):
        # The val passed to a callback by the RangeSlider will
        # be a tuple of (min, max)

        # Update the image's colormap
        im.norm.vmin = val[0]
        im.norm.vmax = val[1]

        # Update the position of the vertical lines
        lower_limit_line.set_xdata([val[0], val[0]])
        upper_limit_line.set_xdata([val[1], val[1]])

        # Redraw the figure to ensure it updates
        fig.canvas.draw_idle()

    def update_x(val):
        # The val passed to a callback by the RangeSlider will
        # be a tuple of (min, max)
        i1 = val[0]
        i2 = val[1]
        axs[0].set_xlim([i1, i2])
        fig.canvas.draw_idle()
    
    def update_y(val):
        # The val passed to a callback by the RangeSlider will
        # be a tuple of (min, max)
        i1 = val[0]
        i2 = val[1]
        axs[0].set_ylim([i2, i1])
        fig.canvas.draw_idle()

    slider.on_changed(update)
    slider_x.on_changed(update_x)
    slider_y.on_changed(update_y)


    divider = make_axes_locatable(axs[0])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)    
    plt.show() 

# import matplotlib.pyplot as plt
# from mpl_toolkits.axes_grid1 import make_axes_locatable
# import numpy as np

# def demo_locatable_axes_easy(ax):
#     from mpl_toolkits.axes_grid1 import make_axes_locatable

#     divider = make_axes_locatable(ax)

#     ax_cb = divider.new_horizontal(size="5%", pad=0.05)
#     fig = ax.get_figure()
#     fig.add_axes(ax_cb)

#     Z, extent = get_demo_image()
#     im = ax.imshow(Z, extent=extent)

#     plt.colorbar(im, cax=ax_cb)
#     ax_cb.yaxis.tick_right()
#     ax_cb.yaxis.set_tick_params(labelright=False)

# ax = plt.subplot()
# im = ax.imshow(np.arange(100).reshape((10, 10)))

# # create an axes on the right side of ax. The width of cax will be 5%
# # of ax and the padding between cax and ax will be fixed at 0.05 inch.
# divider = make_axes_locatable(ax)
# cax = divider.append_axes("right", size="5%", pad=0.05)

# plt.colorbar(im, cax=cax)

# plt.show()